const form = document.getElementById('form')
const name = document.getElementById('name')

const names = []

const validiteForm = (e) => {
    e.preventDefault()
    if (name.value === '' || name.value == null) {
        console.log('name is required')
    } else if (name.value.length < 5) {
        console.log('name must be greater than 6')
    } else {
        names.push(name.value)
        console.log(names)
    }
}

form.addEventListener('submit', validiteForm)

// document.createElement
// innerHTML
// append function in js